import React from "react"
import {MenuItem as BaseMenuItem, menuItemClasses, MenuList, Paper, Stack} from "@mui/material"
import {Link} from "react-router-dom"
import {styled} from "@mui/system"

const LeftMenu = () => {
    const menu = [
        {
            link: '/',
            label: 'Основная информация'
        },
        {
            link: '/work',
            label: 'Опыт работы'
        },
        {
            link: '/education',
            label: 'Образование'
        },
        {
            link: '/overview',
            label: 'Обзор'
        }
    ]

    const createHandleMenuClick = (menuItem: string) => {
        return () => {
            console.log(`Clicked on ${menuItem}`);
        };
    };
    return (
        <Stack direction="row" spacing={2}>
            <Paper>
                <MenuList>
                    {menu.map((option) => (
                        <Link to={option.link} key={option.link} style={{textDecoration: 'none'}}>
                            <MenuItem onClick={createHandleMenuClick(option.link)} key={option.link}
                                      value={option.label}>
                                {option.label}
                            </MenuItem>
                        </Link>
                    ))}
                </MenuList>
            </Paper>
        </Stack>
    )
}
export default LeftMenu

const blue = {
    50: '#F0F7FF',
    100: '#C2E0FF',
    200: '#99CCF3',
    300: '#66B2FF',
    400: '#3399FF',
    500: '#007FFF',
    600: '#0072E6',
    700: '#0059B3',
    800: '#004C99',
    900: '#003A75',
};

const grey = {
    50: '#F3F6F9',
    100: '#E5EAF2',
    200: '#DAE2ED',
    300: '#C7D0DD',
    400: '#B0B8C4',
    500: '#9DA8B7',
    600: '#6B7A90',
    700: '#434D5B',
    800: '#303740',
    900: '#1C2025',
}
const MenuItem = styled(BaseMenuItem)(
    ({theme}) => `
  list-style: none;
  padding: 8px;
  border-radius: 4px;
  cursor: default;
  user-select: none;

  &:last-of-type {
    border-bottom: none;
  }

  &.${menuItemClasses.focusVisible} {
    outline: 3px solid ${theme.palette.mode === 'dark' ? blue[600] : blue[200]};
    background-color: ${theme.palette.mode === 'dark' ? grey[800] : grey[100]};
    color: ${theme.palette.mode === 'dark' ? grey[300] : grey[900]};
  }

  &.${menuItemClasses.disabled} {
    color: ${theme.palette.mode === 'dark' ? grey[700] : grey[400]};
    cursor: pointer;
  }

  &:hover:not(.${menuItemClasses.disabled}) {
      cursor: pointer;
    background-color: ${theme.palette.mode === 'dark' ? blue[900] : blue[50]};
    color: ${theme.palette.mode === 'dark' ? blue[100] : blue[800]};
  }
  `,
)