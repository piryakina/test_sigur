import React, {ChangeEvent, useContext, useState} from "react"
import {TextField} from "@mui/material"
import {observer} from "mobx-react"
import {storeContext} from "../../Stores/InfoStore/InfoStore"

interface IInput {
    readonly required: boolean,
    readonly type?: string,
    readonly label: string,
    readonly name: string,
    readonly value?: string,
    readonly storagePath: Array<string>
}

const InputItem = observer((props: IInput) => {
    const [error, setError] = useState(props.required)
    const [helpText,setHelp] = useState('')
    function updateProperty(val: string) {
        store.setParam(props.storagePath, props.name, val)
    }
    const store = useContext(storeContext)
    const handleTextFieldChange = (event: ChangeEvent<HTMLInputElement>) => {
        if (currentValue && (currentValue == '' || currentValue.length<3) ){
            setError(true)
            setHelp('В строке должно быть больше 3 символов!')
        } else {
            setError(false )
            setHelp('')
        }
        updateProperty(event.target.value)
    };
    const currentValue = store.getParam(props.storagePath, props.name).value
    return (
        <TextField helperText={helpText} error={error} fullWidth sx={{mt: 0}} margin={'normal'} variant={'outlined'} required={props.required}
                   label={props.label} type={props.type} onChange={handleTextFieldChange} value={currentValue}/>
    )
})
export default InputItem
