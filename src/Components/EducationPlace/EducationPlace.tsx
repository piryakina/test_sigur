import React from "react"
import {Button, Grid} from "@mui/material"
import InputItem from "../InputItem/InputItem"
import DeleteOutlinedIcon from "@mui/icons-material/DeleteOutlined"
import {NumericFormat} from "react-number-format";
const EducationPlace = () =>{
    return(
        <><Grid  container spacing={3}>
            <Grid item xs={12}>
                <InputItem name={'eduPlace'} required={true} label={'Название учебного заведения'} storagePath={['eduLayer']}/>
                <InputItem name={'faculty'} required={true} label={'Факультет'} storagePath={['eduLayer']}/>
                <InputItem name={'specialize'} required={true} label={'Специализация'} storagePath={['eduLayer']}/>
                <Grid style={{marginBottom:'3px'}} container spacing={3}>
                    <Grid item xs={6}>
                        <NumericFormat
                            value={12323}
                            prefix="$"
                            thousandSeparator
                            customInput={InputItem} name={'finishYear'} required={true} label={'Год окончания'} storagePath={['eduLayer']}/>
                        {/*<InputItem name={'finishYear'} required={true} label={'Год окончания'} type={'number'} storagePath={['eduLayer']}/>*/}
                    </Grid>
                    <Grid item xs={6}  display={'flex'} lineHeight={'38px'}
                          justifyContent={'space-around'}>
                        <Button fullWidth style={{marginBottom:'8px'}} variant="outlined">Удалить <DeleteOutlinedIcon/></Button>
                    </Grid>
                </Grid>
            </Grid>
        </Grid></>
    )
}
export default EducationPlace