import React, {useContext} from "react"
import {observer} from "mobx-react"
import {storeContext} from "../../Stores/InfoStore/InfoStore"
import RowItem from "../RowItem/RowItem"
import {Grid} from "@mui/material"

const OverviewItem = observer(() => {
    const store = useContext(storeContext)
    const res = store.getParams()
    return (
        <>
            <Grid width={1000} container spacing={3} style={{border:'1px solid green'}}>
                <Grid item xs={4} style={{border:'1px solid green'}} >
                    {
                        Object.entries(res.aboutLayer).map(([key, {label, value}]) => {
                            console.log(key);
                            console.log(label);
                            console.log(value);
                            return (
                                <RowItem key={key} label={label} value={value}/>
                            )
                        })
                    }
                </Grid>
                <Grid item xs={4} style={{border:'1px solid green'}}>
                    {
                        Object.entries(res.eduLayer).map(([key, {label, value}]) => {
                            console.log(key);
                            console.log(label);
                            console.log(value);
                            return (
                                <RowItem key={key} label={label} value={value}/>
                            )
                        })
                    }
                </Grid>
                <Grid item xs={4} style={{border:'1px solid green'}}>
                    {
                        res.workLayer.map((item, index) => {
                            return Object.entries(item).map(([key, {label, value}]) => {
                                return (
                                    <RowItem key={`workLayer_${index}_${key}`} label={label} value={value}/>
                                )
                            })
                        }).flat()
                    }
                </Grid>
            </Grid>
        </>
    )
})
export default OverviewItem