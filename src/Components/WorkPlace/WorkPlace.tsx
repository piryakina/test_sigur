import InputItem from "../InputItem/InputItem"
import {Button, FormControlLabel, Grid, Switch} from "@mui/material"
import React, {ChangeEvent, FormEvent, useState} from "react"
import DeleteOutlinedIcon from "@mui/icons-material/DeleteOutlined"
import {DatePicker, LocalizationProvider} from "@mui/x-date-pickers"
import {AdapterDayjs} from "@mui/x-date-pickers/AdapterDayjs"
import dayjs from "dayjs"

export interface IWork {
    dbIndex: number,
    onDelete: any
}

const WorkPlace = (props: IWork) => {
    // const [countWorkPlaces, setCount] = useState([0])
    const [today, setData] = useState(true)
    const handleFinish = (event: ChangeEvent<HTMLInputElement>) => {
        setData(event.target.checked)
    }
    const handleDelete = (event: FormEvent<HTMLButtonElement>) => {
        props.onDelete(props.dbIndex) // callback-функция
    }
    // const handleDeleteWorkPlace = () => {
    //     // if (countWorkPlaces.pop() !== undefined) {
    //     //     setCount([countWorkPlaces.pop() ?? 0])
    //     // }
    // }
    // const currentValue = store.getParam(['workLayer', props.dbIndex.toString()], 'startWork').value
    const currentValue = dayjs(new Date())
    const handleDateStartChange = (event: any) => {
        console.log(currentValue)
        // updateProperty(event.target.value)
    };
    // function updateProperty(val: string) {
    //     store.setParam(['workLayer', props.dbIndex.toString()], 'startWork', currentValue)
    // }
    const min = dayjs().set('year', 1990)
    return (
        <Grid container spacing={1}>
            <Grid item xs={12}>
                <InputItem name={'workPlace'} required={true} label={'Место работы'}
                           storagePath={['workLayer', props.dbIndex.toString()]}/>
                <InputItem name={'job'} required={true} label={'Должность'}
                           storagePath={['workLayer', props.dbIndex.toString()]}/>
                <InputItem name={'charge'} required={false} label={'Обязанности'}
                           storagePath={['workLayer', props.dbIndex.toString()]}/>
            </Grid>
            <Grid item xs={6}>
                {/*<InputItem name={'startWork'} required={true} label={'Начало работы'} storagePath={['workLayer', props.dbIndex.toString()]}/>*/}
                <LocalizationProvider dateAdapter={AdapterDayjs}>
                    <DatePicker key={'startWork'} onChange={(event)=>{handleDateStartChange(event)}} disableFuture minDate={min} label={'Начало работы'} views={['month', 'year']} format={'MM/YY'} sx={{width: '100%'}}
                                defaultValue={dayjs('06/1990')} value={currentValue}/>
                </LocalizationProvider>
            </Grid>
            <Grid item xs={6}>
                <FormControlLabel style={{justifyContent: 'flex-end'}}
                                  value="start"
                                  control={<Switch checked={today} onChange={handleFinish}/>}
                                  label="По настоящее время"
                                  labelPlacement="start"
                />
            </Grid>
            <Grid item xs={6}>
                {
                    !today ? <>
                        {/*<Grid minWidth={120} item width={100} xs={4}>*/}
                        <LocalizationProvider dateAdapter={AdapterDayjs}>
                            <DatePicker key={'finishWork'} minDate={min} label={'Окончание'} views={['month', 'year']} format={'MM/YY'} sx={{width: '100%'}}
                                        defaultValue={dayjs('01-06-2000')}/>
                        </LocalizationProvider>
                        {/*</Grid>*/}
                        {/*<InputItem name={'finishWork'} required={true} label={'Окончание'} storagePath={['workLayer', props.dbIndex.toString()]}/>*/}
                    </> : <></>
                }
            </Grid>
            <Grid item xs={6}></Grid>
            <Grid item xs={6} display={'flex'} lineHeight={'38px'}
                  justifyContent={'space-around'} paddingBottom={'20px'}>
                <Button onClick={(e) => {
                    handleDelete(e)
                }} fullWidth variant="outlined">Удалить <DeleteOutlinedIcon/></Button>
            </Grid>


        </Grid>
    )
}

export default WorkPlace