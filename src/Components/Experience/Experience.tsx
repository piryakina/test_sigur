import React, {useContext, useState} from "react"
import {
    Button, FormControl, FormControlLabel,
    Grid, Radio, RadioGroup,
    Typography
} from "@mui/material"
import AddIcon from '@mui/icons-material/Add'
import WorkPlace from "../WorkPlace/WorkPlace"
import {storeContext} from "../../Stores/InfoStore/InfoStore"

const Experience = () => {
    const store = useContext(storeContext)
    const updateActualWorksLength = () => {
        setWorkCounter(store.getParam([],'workLayer').length ?? 0)
        console.log(workCounter)
    }
    const countLengthOnMount = store.getParam([],'workLayer')?.length ?? 0
    const countArrayOnMount = new Array(countLengthOnMount).fill(0)
    const [countWorkPlaces, setCount] = useState(countArrayOnMount)
    const [checked, setChecked] = useState(false)
    const [workCounter, setWorkCounter] = useState(store.getParam([],'workLayer')?.length ?? 0)
    const handleChange = (dbIndex: number) => {
        // setValue(value)
        console.log(dbIndex)
        setCount([...countWorkPlaces.slice(1)])
        store.removeFromArray([], 'workLayer', dbIndex)
    }
    const handleAddWorkPlace = () => {
        // @ts-ignore
        setCount([...countWorkPlaces, 0])
        store.addArrayParam([], 'workLayer', store.generate.workLayer())
        updateActualWorksLength()
    }
    const handleChecked = () => {
        if (!countWorkPlaces.length){
            handleAddWorkPlace()
        }
        setChecked(!checked)
    }
    return (
        <FormControl>
            <Grid width={1000} spacing={3} minWidth={120} minHeight={30} alignContent={'left'}
                  alignItems={'flex-start'}
                  justifyContent={'left'} padding={0} container>
                <Grid item xs={4}>
                    <Typography variant={'h6'} textAlign={'start'}>Опыт работы</Typography>
                    <RadioGroup defaultValue={false} onChange={handleChecked}>
                        <FormControlLabel value={true} control={<Radio/>} label="Есть"/>
                        <FormControlLabel value={false} control={<Radio/>} label="Нет"/>
                    </RadioGroup>
                </Grid>
                {checked && countWorkPlaces ? <>
                        {countWorkPlaces.map((_, index)=>(<WorkPlace onDelete={handleChange} dbIndex={index}/>))}
                        <Grid container spacing={1}>
                            <Grid item xs={6} onClick={handleAddWorkPlace} display={'flex'} lineHeight={'40px'}
                                  justifyContent={'space-around'}>
                                <Button fullWidth variant="outlined">Добавить<AddIcon/></Button>
                            </Grid>
                            {/*<Grid item xs={6} onClick={()=>{handleDeleteWorkPlace(1)}} display={'flex'} lineHeight={'40px'}*/}
                            {/*      justifyContent={'space-around'}>*/}
                            {/*    <Button fullWidth variant="outlined">Удалить <DeleteOutlinedIcon/></Button>*/}
                            {/*</Grid>*/}
                        </Grid>
                    </>
                    :
                    <></>
                }
            </Grid>
        </FormControl>
    )
}
export default Experience