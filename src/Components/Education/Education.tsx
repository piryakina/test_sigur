import React, {useState} from 'react'
import DropDown from "../DropDown/DropDown"
import { FormControl, Grid, Typography} from "@mui/material"

import EducationPlace from "../EducationPlace/EducationPlace"

const edu = [
    {
        label: 'Без образования',
        value: 'none'
    },
    {
        label: 'Среднее',
        value: 'middle'
    },
    {
        label: 'Среднее специальное',
        value: 'middlesp'
    },
    {
        label: 'Бакалавриат',
        value: 'bachelor'
    },
    {
        label: 'Специалитет',
        value: 'specialist'
    },
    {
        label: 'Магистратура',
        value: 'master'
    }
]
const language = [
    {
        label: 'Русский',
        value: 'russian'
    },
    {
        label: 'Казахский',
        value: 'kazakh'
    },
    {
        label: 'Английский',
        value: 'english'
    },
    {
        label: 'Немецкий',
        value: 'turkish'
    },
]
const level = [
    {
        label: 'A1',
        value: 'a1'
    },
    {
        label: 'A2',
        value: 'a2'
    },
    {
        label: 'B1',
        value: 'b1'
    },
    {
        label: 'B2',
        value: 'b2'
    },
    {
        label: 'C1',
        value: 'C1'
    },
    {
        label: 'C2',
        value: 'c2'
    },
]


const Education = () => {
    const [eduPlaces, setEdu] = useState([0])
    // const handleAddWorkPlace = () => {
    //     setEdu([...eduPlaces, 0])
    // }
    const handleDeleteWorkPlace = () => {
        if (eduPlaces.pop() !== undefined) {
            setEdu([eduPlaces.pop() ?? 0])
        }
    }
    return (
        <FormControl>
            <Grid width={1000} spacing={3} minWidth={120} minHeight={30} alignContent={'center'}
                  alignItems={'flex-start'}
                  justifyContent={'left'} padding={0} container>
                <Grid item xs={4}>
                    <Typography variant={'h6'} textAlign={'start'}>Образование</Typography>
                    <DropDown label={'Уровень образования'} items={edu}/>
                </Grid>
                <Grid item xs={4}>
                    <Typography variant={'h6'} textAlign={'start'}>Языки</Typography>
                    <DropDown label={'Родной язык'} items={language}/>
                </Grid>
                <Grid item xs={4}/>
                <Grid item xs={4}>
                    <Typography variant={'subtitle1'} textAlign={'start'}>Место обучения</Typography>
                    {eduPlaces ? <>
                            {eduPlaces.map(x => <EducationPlace/>)}
                        </>
                        : <></>
                    }
                </Grid>
                <Grid item xs={4}>
                    <Typography variant={'subtitle1'} textAlign={'start'}>Иностранные языки</Typography>
                    <Grid container spacing={1} sx={{pt:'0'}}>
                        <Grid xs={6} item>
                            <DropDown label={'Иностранный язык'} items={language}/>
                        </Grid>
                        <Grid xs={6} item>
                            <DropDown label={'Уровень знания'} items={level}/>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </FormControl>
    )
}

export default Education