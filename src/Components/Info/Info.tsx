import {FormControl, Grid, TextareaAutosize, Typography} from "@mui/material"
import InputItem from "../InputItem/InputItem"
import {DatePicker, LocalizationProvider} from "@mui/x-date-pickers"
import {AdapterDayjs} from "@mui/x-date-pickers/AdapterDayjs"
import dayjs from "dayjs"
import DropDown from "../DropDown/DropDown"
import React from "react"
import {styled} from "@mui/system"

const gender = [
    {
        value: 'man',
        label: 'Мужской'
    },
    {
        value: 'woman',
        label: 'Женский'
    }
]
const currencies = [
    {
        value: 'RUB',
        label: 'Рубли',
    },
    {
        value: 'USD',
        label: 'Доллары',
    },
    {
        value: 'EUR',
        label: 'Евро',
    },
];
const Info = () => {
    return (
        <FormControl>
            <Typography variant={'h6'} textAlign={'start'}>Основная информация</Typography>
            <Grid container={true} width={1000} spacing={3} minWidth={120} minHeight={30} alignContent={'center'}
                  alignItems={'flex-start'}
                  justifyContent={'left'} padding={0}>
                <Grid minWidth={120} item xs={4} padding={0}>
                    <InputItem name={'surname'} label={'Фамилия'} type={'text'} required={true} storagePath={['aboutLayer']}/>
                </Grid>
                <Grid minWidth={120} item xs={4} padding={0}>
                    <InputItem name={'name'} label={'Имя'} type={'text'} required={true} storagePath={['aboutLayer']}/>
                </Grid>
                <Grid minWidth={120} item xs={4}>
                    <InputItem name={'secondName'} label={'Отчество'} type={'text'} required={false} storagePath={['aboutLayer']}/>
                </Grid>
                <Grid minWidth={120} item width={100} xs={4}>
                    <LocalizationProvider dateAdapter={AdapterDayjs}>
                        <DatePicker disableFuture label="Дата рождения" format={'L'} sx={{width: '100%'}} defaultValue={dayjs('01-06-2000')}/>
                    </LocalizationProvider>
                </Grid>
                <Grid minWidth={120} item xs={3}>
                    <DropDown label={'Пол'} items={gender}/>
                </Grid>
                <Grid minWidth={120} item xs={5}>
                    <InputItem name={'city'} label={'Город проживания'} type={'text'} required={true} storagePath={['aboutLayer']}/>
                </Grid>
                <Grid minWidth={120} item xs={7}>
                    <InputItem name={'citizenship'} required={true} label={'Гражданство'} storagePath={['aboutLayer']}/>
                </Grid>
                <Grid minWidth={120} item xs={6}>
                    <InputItem name={'jobTitle'} required={true} label={'Желаемая должность'} storagePath={['aboutLayer']}/>
                </Grid>
                <Grid item minWidth={120} xs={4}>
                    <InputItem name={'currency'}  required={true} label={'Желаемая зарплата'} type={'number'} storagePath={['aboutLayer']}/>
                </Grid>
                <Grid item minWidth={120} xs={2}>
                    <DropDown label={'Валюта'} items={currencies}/>
                </Grid>
                <Grid minWidth={120} item xs={6}>
                    <Textarea sx={{width: '100%'}} aria-label="minimum height" minRows={3} placeholder="О себе"/>
                </Grid>
            </Grid>
        </FormControl>
    )
}

const Textarea = styled(TextareaAutosize)(
    ({theme}) => `
    font-family: 'IBM Plex Sans', sans-serif;
    font-size: 0.875rem;
    font-weight: 400;
    line-height: 1.5;
    border-radius: 8px;
    color: ${theme.palette.mode === 'dark' ? grey[300] : grey[900]};
    background: ${theme.palette.mode === 'dark' ? grey[900] : '#fff'};
    border: 1px solid ${theme.palette.mode === 'dark' ? grey[700] : grey[200]};
    box-shadow: 0px 2px 2px ${theme.palette.mode === 'dark' ? grey[900] : grey[50]};
    &:hover {
      border-color: ${blue[400]};
    }
    &:focus {
      border-color: ${blue[400]};
      box-shadow: 0 0 0 3px ${theme.palette.mode === 'dark' ? blue[600] : blue[200]};
    }
    // firefox
    &:focus-visible {
      outline: 0;
    }
  `,
);
const blue = {
    100: '#DAECFF',
    200: '#b6daff',
    400: '#3399FF',
    500: '#007FFF',
    600: '#0072E5',
    900: '#003A75',
};

const grey = {
    50: '#F3F6F9',
    100: '#E5EAF2',
    200: '#DAE2ED',
    300: '#C7D0DD',
    400: '#B0B8C4',
    500: '#9DA8B7',
    600: '#6B7A90',
    700: '#434D5B',
    800: '#303740',
    900: '#1C2025',
};

export default Info