import React, {useState} from "react"
import {Box, FormControl, InputLabel, MenuItem, Select, SelectChangeEvent} from "@mui/material"

interface IDropDown {
    readonly label: string,
    readonly items: { value: string, label: string }[]
}

const DropDown = (props: IDropDown) => {
    const [selected, setSelected] = useState('')
    const handleChange = (event: SelectChangeEvent) => {
        setSelected(event.target.value as string);
    };
    return (
        <Box margin={'normal'} sx={{minWidth: 100}}>
            <FormControl fullWidth>
                <InputLabel>{props.label}</InputLabel>
                <Select
                    id="outlined-select-currency"
                    label={props.label}
                    value={selected}
                    autoWidth={true}
                    onChange={handleChange}
                >
                    {props.items.map((option) => (
                        <MenuItem key={option.value} value={option.label}>
                            {option.label}
                        </MenuItem>
                    ))}
                </Select>
            </FormControl>
        </Box>
    )

}
export default DropDown