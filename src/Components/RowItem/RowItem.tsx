import React from "react"
import {Grid, Typography} from "@mui/material"

interface IRow{
    label: string,
    value: any
}

const RowItem = (props: IRow) => {
    return (
        <Grid container>
            <Grid item xs={6}>
                <Typography variant={'subtitle1'} textAlign={'start'}>{props.label} : </Typography>
            </Grid>
            <Grid item xs={6}>
                <Typography variant={'subtitle1'} textAlign={'start'}>{props.value}</Typography>
            </Grid>
        </Grid>
    )
}
export default RowItem