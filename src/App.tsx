import React from 'react'
import './App.css'
import MainInfo from "./Pages/MainInfo/MainInfo"
import {Route, Routes} from "react-router-dom"
import WorkExp from "./Pages/WorkExp/WorkExp"
import EducationInfo from "./Pages/EducationInfo/EducationInfo"
import Overview from "./Pages/Overview/Overview"

function App() {
    return (
        <div className="App">
            <Routes>
                <Route path="" element={<MainInfo/>}/>
                <Route path="work" element={<WorkExp/>}/>
                <Route path="education" element={<EducationInfo/>}/>
                <Route path="overview" element={<Overview/>}/>
            </Routes>
        </div>
  );
}

export default App;
