import {makeAutoObservable} from "mobx"
import {createContext} from "react"

interface IElement<T> {
    label: string,
    value: T
}
export interface IWorkInfo{
    job: IElement<string>,
    startWork: IElement<string>,
    finishWOrk: IElement<string>,
    workPlace: IElement<string>,
    charge: IElement<string>,
    salary: IElement<number>,
}

interface IInfo {
    workLayer: Array<IWorkInfo>,
    aboutLayer: {
        city: IElement<string>,
        surname: IElement<string>,
        name: IElement<string>,
        secondName: IElement<string>,
        dateOfBirth: IElement<string>,
        gender: IElement<string>,
        about?: IElement<string>,
        citizenship: IElement<string>,
        jobTitle: IElement<string>,
        currency: IElement<string>,
    },
    eduLayer:{
        eduPlace: IElement<string>,
        faculty: IElement<string>,
        specialize: IElement<string>,
        finishYear: IElement<number>,
    }
}

class ObservableInfoStore {

    generate = {
        workLayer: ()=>({
            workPlace: {label: "Место работы", value: ""},
            startWork: {label: "Дата начала работы", value: ""},
            finishWOrk: {label: "Дата окончания работы", value: ""},
            job: {label: "Должность", value: ""},
            charge: {label: "Обязанности", value: ""},
            salary: {label: "Зарплата", value: -2},
        }),
        aboutLayer: ()=>({
            surname: {label: "Фамилия", value: ""},
            name: {label: "Имя", value: ""},
            secondName: {label: "Отчество", value: ""},
            dateOfBirth: {label: "День рождения ", value: ""},
            gender: {label: "Пол", value: ""},
            city: {label: "Город", value: ""},
            citizenship: {label: "Гражданство", value: ""},
            jobTitle: {label: "Желаемая должность", value: ""},
            currency: {label: "Валюта", value: ""},
            about: {label: "О себе", value: ""},

        }),
        eduLayer: ()=>({
            eduPlace: {label: "Место обучения", value: ""},
            faculty: {label: "Факультет", value: ""},
            specialize: {label: "Специальность", value: ""},
            finishYear: {label: "Дата окончания ВУЗа", value: 1980},

        })
    }
    info: IInfo = {
        // workLayer: [this.generate.workLayer()],
        workLayer: [],
        aboutLayer: this.generate.aboutLayer(),
        eduLayer:this.generate.eduLayer()
    }
    constructor() {
        makeAutoObservable(this)
    }
    setParam(propertyPath: Array<string>, key: string, name: any) {
        const prop = this.getProp(propertyPath)
        // @ts-ignore
        prop[key as keyof IInfo].value = name
        const a = this.info.workLayer[1] === this.info.workLayer[2]
        console.log(a)
    }
    getParams() {
        return this.info
    }
    getProp(propertyPath: Array<string>){
        let prop = this.info
        propertyPath.forEach((path)=>{
            // @ts-ignore
            prop = prop[path as keyof IInfo]})
        return prop
    }
    getParam(propertyPath: Array<string>, key: string){
        const prop = this.getProp(propertyPath)
        if (prop){
            // @ts-ignore
            return prop[key]
        }
    }

    addArrayParam(propertyPath: Array<string>, key: string, data: unknown){
        const array = this.getParam(propertyPath, key)
        if (Array.isArray(array)){
            array.push(data)
        }
    }

    removeFromArray(propertyPath: Array<string>, key: string, index: number){
        const array = this.getParam(propertyPath, key)
        if (!Array.isArray(array) || !array.at(index)){
            return
        }
        array.splice(index, 1)
    }
}
export const store = new ObservableInfoStore()
export const storeContext = createContext(store)
