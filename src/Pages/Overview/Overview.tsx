import React from "react"
import {Grid} from "@mui/material"
import LeftMenu from "../../Components/LeftMenu/LeftMenu"
import OverviewItem from "../../Components/OverviewItem/OverviewItem"

const Overview = () =>{
    return (
        <Grid width={1000} spacing={3} minWidth={120} minHeight={30} alignContent={'left'}
              alignItems={'flex-start'}
              justifyContent={'left'} padding={0} container>
            <Grid item xs={1}></Grid>
            <Grid minWidth={200} item xs={1} >
                <LeftMenu/>
            </Grid>
            <Grid item xs={8}>
                <OverviewItem/>
            </Grid>
        </Grid>
    )
}
export default Overview
