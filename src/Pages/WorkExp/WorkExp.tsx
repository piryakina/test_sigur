import React from "react"
import {Grid} from "@mui/material"
import LeftMenu from "../../Components/LeftMenu/LeftMenu"
import Experience from "../../Components/Experience/Experience"

const WorkExp = () => {
    return (
        <Grid container  sx={{pt:'20px'}} alignItems={'flex-start'} spacing={3} justifyContent={'left'} justifyItems={'self-start'}>
            <Grid item xs={1}></Grid>
            <Grid minWidth={200} item xs={1} >
                <LeftMenu/>
            </Grid>
            <Grid item xs={8}>
                <Experience/>
            </Grid>
        </Grid>
    )
}

export default WorkExp