import React from "react"
import {Grid} from "@mui/material"
import LeftMenu from "../../Components/LeftMenu/LeftMenu"
import Education from "../../Components/Education/Education"

const EducationInfo = () =>{
    return (
        <Grid container sx={{pt:'20px'}} alignItems={'flex-start'}>
            <Grid item xs={1}></Grid>
            <Grid item xs={1} minWidth={200}>
                <LeftMenu/>
            </Grid>
            <Grid  item xs={8}>
                <Education />
            </Grid>
        </Grid>
    )
}
export default EducationInfo
